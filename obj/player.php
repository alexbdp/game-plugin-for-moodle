<?php

// class that represent a player of the plugin
class player {
	
	public $name; // the name of the player
	public $playerid; // the id of the player in the players table
	public $userid; // the id of the player in the users table
	public $gameid; // the id of the course the player is in
	public $score; // the score of player

}
