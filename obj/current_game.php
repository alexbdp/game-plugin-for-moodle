<?php

// class that represent games registred on bd
class current_game {
	
	public $name; // the name of the player
	public $gameid; // the id of the player in the users table
	public $gametypeid; // the id of the course the player is in
	public $gamecourseid; // the id of relation between current course and game
}
