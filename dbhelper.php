<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

// path to the root
$tags = parse_ini_file(__DIR__ . "/config.ini"); 
$root = $tags['root'];

// error logging
ini_set('date.timezone', 'America/Sao_paulo');
ini_set("log_errors", 1);
ini_set("error_log", $root . '/blocks/games/log/db-error.log');

$dbhelper_file = $root . '/blocks/games/dbhelper.php';

// class responsable for handling db methods and operations
class dbHelper {
	
	// The database connection 
    protected $connection;
	
	
	/**
     * Connect to the database provided in dbconfig.ini
     * 
     * @return bool false on failure / mysqli MySQLi object instance on success
     */
	public function db_connect()
	{
		// Try and connect to the database
        if(!isset($this->connection)) {
            // Load configuration as an array. Use the actual location of your configuration file
            $config = parse_ini_file('db/dbconfig.ini'); 
            $this->connection = new mysqli($config['host'],$config['username'],$config['password'],$config['dbname']);
        }

        // If connection was not successful, handle the error
        if($this->connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
			echo 'Could not connect to the database. Check your database config file.';
            return  mysqli_connect_error(); 
        }
        return $this->connection;
	}
	
	/**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function db_query($query) {
        // Connect to the database
        $connection = $this->db_connect();

        // Query the database
        $result = $connection -> query($query);

        return $result;
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function db_select($query) {
        $rows = array();
        $result = $this -> db_query($query);
        if($result === false) {
            return false;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function db_error() {
        $connection = $this -> db_connect();
        return $connection -> error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function db_quote($value) {
        $connection = $this -> db_connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }
	
	// creates the table that will hold of types of games installed on the block,
	//later to be registered by db_register_game_type
	public function db_create_table_game_type() {
		
		// Connect to the database
		$connection = $this->db_connect();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if it already exists
		$query = "SELECT * FROM `".$prefix."game_type`";
		$result = $this->db_query($query);

		// if table do not exist yet, creates the table
		if(empty($result)) {
			$query = "CREATE TABLE IF NOT EXISTS `".$prefix."game_type` (
					  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					  name VARCHAR(45))";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_create_table_game_type())<br>Error? '.$error;
				return false;
			}
		}
	}

	// creates the table that will hold all games created,
	//later to be registered by db_register_game
	public function db_create_table_game() {
		
		// Connect to the database
		$connection = $this->db_connect();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if it already exists
		$query = "SELECT * FROM `".$prefix."game`";
		$result = $this->db_query($query);

		// if table do not exist yet, creates the table
		if(empty($result)) {
			$this->db_create_table_game_type();
			$query = "CREATE TABLE IF NOT EXISTS `".$prefix."game` (
					  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					  name VARCHAR(145),
                      game_type_id INT NOT NULL,
                      deleted  BOOLEAN NOT NULL DEFAULT 0,
                      CONSTRAINT fk_game_type_id FOREIGN KEY (game_type_id) REFERENCES `".$prefix."game_type`(id) ON DELETE CASCADE)";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_create_table_game())<br>Error? '.$error;
				return false;
			}
		}
	}

	// creates the table that will do the relation between games created
	// and courses, later to be registered by db_register_game_course
	public function db_create_table_game_course() {
		
		// Connect to the database
		$connection = $this->db_connect();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if it already exists
		$query = "SELECT * FROM `".$prefix."game_course`";
		$result = $this->db_query($query);

		// if table do not exist yet, creates the table
		if(empty($result)) {
			$this->db_create_table_game();
			$query = "CREATE TABLE IF NOT EXISTS `".$prefix."game_course` (
					  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					  deleted BOOLEAN NOT NULL DEFAULT 0,
                      game_id INT NOT NULL,
                      course_id BIGINT(10) NOT NULL,
                      CONSTRAINT fk_game_id FOREIGN KEY (game_id) REFERENCES `".$prefix."game`(id) ON DELETE CASCADE,
                      CONSTRAINT fk_course_id FOREIGN KEY (course_id) REFERENCES `".$prefix."course`(id) ON DELETE CASCADE)";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_create_table_game_course())<br>Error? '.$error;
				return false;
			}
		}
	}


	// creates the table that will do the relation between all players
	// and games of the course, later to be registered by db_register_player
	public function db_create_table_players() {
		
		// Connect to the database
		$connection = $this->db_connect();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if it already exists
		$query = "SELECT * FROM `".$prefix."game_player`";
		$result = $this->db_query($query);

		// if table do not exist yet, creates the table
		if(empty($result)) {
			$this->db_create_table_game_course();
			$query = "CREATE TABLE IF NOT EXISTS `".$prefix."game_player` (
					  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      game_course_id INT NOT NULL,
                      user_id BIGINT(10) NOT NULL,
                      score INT(10) NOT NULL DEFAULT 0,
                      CONSTRAINT fk_game_course_id FOREIGN KEY (game_course_id) REFERENCES `".$prefix."game_course`(id) ON DELETE CASCADE,
                      CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES `".$prefix."user`(id) ON DELETE CASCADE)";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_create_table_players())<br>Error? '.$error;
				return false;
			}
		}
	}
	
	// creates the table that will do the relation between games
	// and quiz created at course, later to be registered by db_register_game_quiz
	public function db_create_table_game_quiz() {
		
		// Connect to the database
		$connection = $this->db_connect();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if it already exists
		$query = "SELECT * FROM `".$prefix."game_quiz`";
		$result = $this->db_query($query);

		// if table do not exist yet, creates the table
		if(empty($result)) {
			$this->db_create_table_game();
			$query = "CREATE TABLE IF NOT EXISTS `".$prefix."game_quiz` (
					  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      game_id INT NOT NULL,
                      quiz_id BIGINT(10) NOT NULL,
                      CONSTRAINT fk_game_quiz FOREIGN KEY (game_id) REFERENCES `".$prefix."game`(id) ON DELETE CASCADE,
                      CONSTRAINT fk_quiz_id FOREIGN KEY (quiz_id) REFERENCES `".$prefix."quiz`(id) ON DELETE CASCADE)";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_create_table_game_quiz())<br>Error? '.$error;
				return false;
			}
		}
	}

	// register a player (plugin user) in the db
	// if player isn`t registered in the current game yet,
	// as soon as a user access the game.
	// a player is a user within a course, which
	// means that the same user will be considered 
	// a different player depending on the course and game
	public function db_register_player($user_id, $game_course_id)
	{
		// Connect to the database
		$connection = $this->db_connect();
		// creates player table if does not exist yet
		$this->db_create_table_players();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if player already exists on the game
		$query = "SELECT * FROM `".$prefix."game_player` WHERE `user_id` = ".$user_id." and `game_course_id`=".$game_course_id."";
		$result = $this->db_select($query);
		// if player isnt registered yet, registers the player
		if(!$result) {
			$query = "INSERT INTO `".$prefix."game_player` (`user_id`,`game_course_id`)
							VALUES (".$user_id.",".$game_course_id.")";
			$result = $this->db_query($query);
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_register_player())<br>Error? '.$error;
				return false;
			}
		}
	}


	
	// returns the player id of a respective
	// user id and current game id (a game player)
	public function db_get_playerID($user_id, $game_course_id) 
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the players class (once, avoiding redeclaration)
		require_once 'obj/player.php';
		
		// selects id(playerid), from a {user_id && game_course_id}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_players = $this->db_select("SELECT `id` 
			FROM `".$prefix."game_player` 
			WHERE `user_id` = ".$user_id." AND `game_course_id` = ".$game_course_id."");

		// error
		if($rows_players === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_playerID())<br>Error? '.$error;
			return false;
		}
		
		return $rows_players[0]['id'];
	}
	
	
	
	// returns an array of multichoice questions 
	// with all info needed for the games
	// require the class of multi choice questions
	public function get_multichoice_questions($category)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the multichoice question class (once, avoiding redeclaration)
		require_once 'obj/multichoice_question.php';
		
		// the array of multichoice questions that will be returned by this function
		$mc_questions = array();
		
		// selects id, name and question text from all multichoice questions
		// to store in the array of multichoice questions
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		if(empty($category))
			$rows_questions = $this->db_select("SELECT `id`, `name`,`questiontext` FROM `".$prefix."question` WHERE `qtype` = 'multichoice'");
		else
			$rows_questions = $this->db_select("SELECT `id`, `name`,`questiontext` 
												FROM `".$prefix."question` 
												WHERE `qtype` = 'multichoice' and `category` = ".$category."");

		// error
		if($rows_questions === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_multichoice_questions())<br>Error? '.$error;
			return false;
		}
		
		// loop through multichoice questions found in the question bank
		foreach($rows_questions as $row_question)
		{
			// creates a new multichoice question that
			// will be added to the array of mc questions
			$mc_question = new multichoice_question();
			// stores known informations
			$mc_question->name = $row_question['name'];
			$mc_question->text = strip_tags($row_question['questiontext']);

			// now that we have the question id
			// we will look for its answers and store it
			$mc_question->answers = array();
			$mc_question->correct_answers_idx = array();
			// we need another sql select now
			$config = parse_ini_file('db/dbconfig.ini'); 
			$prefix = $config['prefix'];
			$rows_answers = $this->db_select("SELECT `id`,`answer`,`fraction` FROM `".$prefix."question_answers` WHERE `question` = ".$row_question['id']."");
			
			// counter of correct answers
			$i = 0;
			// loop through each multichoice questions answers found in the bank
			foreach($rows_answers as $row_answer)
			{
				// adds answer to respective positions in the array of answers (starting from 0)
				$mc_question->answers[($row_answer['id'] - 1)%(count($rows_answers))] = strip_tags($row_answer['answer']);
				
				// if answer is the correct answer, stores the correct id of the array of answers in the correct answers array
				if(($row_answer['fraction'] - 1.0) >= 0)
				{
					$mc_question->correct_answers_idx[$i] = ($row_answer['id'] - 1)%(count($rows_answers));
					$i++;
				}
			}
			// adds the question, with all its information, to the array of questions
			$mc_questions[] = $mc_question;
		}
		// now we have all the info needed. return the array
		return $mc_questions;
	}
	
	// returns the list of categories in the question bank
	// categories ['category'] <=> category id
	public function db_get_categories() 
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		// the array of categories questions that will be returned by this function
		$categories = array();

		// selects category id from distinct categories
		// to store in the array of categories
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];

		$categories = $this->db_select("SELECT DISTINCT `category` FROM `".$prefix."question`");

		// error
		if($rows_questions === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_categories())<br>Error? '.$error;
			return false;
		}

		return $categories;
	}
	

	// receives user id and returns user name
	public function db_get_user_name($user_id)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$query = "SELECT `firstname`,`lastname` FROM `".$prefix."user` WHERE `id` = ".$user_id;
		$row_cname = $this->db_select($query);
		
		// error
		if($row_cname === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_user_name())<br>Error? '.$error;
			return false;
		}
		
		return $row_cname[0]['firstname'] . " " . $row_cname[0]['lastname'];
	}
	
	// receives user player id and returns user name
	public function db_get_player_name($player_id)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		// selects id(playerid), from a {userid && courseid}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_players = $this->db_select("SELECT `u.firstname`,`u.lastname`
			join `".$prefix."games_players`
			FROM `".$prefix."games_players` as gp 
			join `".$prefix."user` as u on u.id=gp.userid 
			WHERE `id` = ".$player_id."");

		
		// error
		if($rows_players === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_player_name()) Error? '.$error);
			return false;
		}
		
		$user_id =  $rows_players[0]['userid'];
		
		return $this->db_get_user_name($user_id);
	}
	
	// receives course id and returns course name
	public function db_get_course_name($course_id)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$query = "SELECT `fullname` FROM `".$prefix."course` WHERE `id` = ".$course_id."";
		$row_cname = $this->db_select($query);
		
		// error
		if($row_cname === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_course_name())<br>Error? '.$error;
			return false;
		}
		
		return $row_cname[0]['fullname'];
	}
	
	// returns the id, on the db, of the current logged user 
	public function get_userID()
	{
		// moodle root dir
		$tags = parse_ini_file(__DIR__ . "/config.ini"); 
		$root = $tags['root'];
		
		// to be able to get user id
		require_once  $root . '/config.php';
		require_login();
		
		$userid = $USER->id;
		
		return $userid;
	}
	
	// returns the id, on the db, of the current course user is in
	public function get_courseID()
	{
		// gets from session cid, registered in the games module
		return $_SESSION['cid'];
	}
	
	// returns a string representing user capability
	// "attempt" / "manage"
	public function get_user_capability()
	{
		// gets user capability registered in session, by the games module
		return $_SESSION['capability'];
	}
	
	// debug
	public function db_print_info()
	{
		$config = parse_ini_file('db/dbconfig.ini'); 

		echo '##### DB INFO #####<br>';
		echo 'host: '.$config['host'].'<br>';
		echo 'username: '.$config['username'].'<br>';
		echo 'password: '.$config['password'].'<br>';
		echo 'dbname: '.$config['dbname'].'<br>';
		echo '###################<br>';
	}

	// register a game type (plugin user) in the db
	// if game type isn`t registered in the db yet,
	// as soon as plugin initiate
	// a game type is register
	public function db_register_game_type($type_name){
		// Connect to the database
		$connection = $this->db_connect();
		
		// creates game type table if does not exist yet
		$this->db_create_table_game_type();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if game type already exists
		$query = "SELECT * FROM `".$prefix."game_type` WHERE `name` = '".$type_name."'";
		$result = $this->db_select($query);

		// if game type isnt registered yet, registers the player
		if(empty($result)) {
			$query = "INSERT INTO `".$prefix."game_type` (name)
							VALUES ('".$type_name."')";
			$result = $this->db_query($query);
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_register_game_type())<br>Error? '.$error;
				return false;
			}
		}

	}


	//this function register a relation between game created and course
	public function db_register_game_course($game_id,$course_id){
		// Connect to the database
		$connection = $this->db_connect();
		// creates game course table if does not exist yet
		$this->db_create_table_game_course();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if game type already exists in the course context 
		$query = "SELECT * FROM `".$prefix."game_course` WHERE `course_id` = ".$course_id." and `game_id`=".$game_id."";
		$result = $this->db_select($query);
		// if game isnt registered yet, registers the game in the course
		if(empty($result)) {
			$query = "INSERT INTO `".$prefix."game_course` (`course_id`,`game_id`)
							VALUES (".$course_id.",".$game_id.")";
			$result = $this->db_query($query);
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_register_game_course())<br>Error? '.$error;
				return false;
			}
		}
	}

	//this function register a new game
	public function db_register_game($type_id, $name_game)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		// creates game table if does not exist yet
		$this->db_create_table_game();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if game already exists
		$query = "SELECT * FROM `".$prefix."game` WHERE `name` = '".$name_game."' AND `game_type_id` = ".$type_id."";
		$result = $this->db_select($query);

		// if player isnt registered yet, registers the player
		if(!($result)) {
			$query = "INSERT INTO `".$prefix."game` (game_type_id,name)
						VALUES (".$type_id.", '".$name_game."')";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_register_game())<br>Error? '.$error;
				return false;
			}
		}
	}


	//this function register a relation between a game created and a quiz
	public function db_register_game_quiz($quiz_id,$game_id){
		// Connect to the database
		$connection = $this->db_connect();
		
		// creates game quiz table if does not exist yet
		$this->db_create_table_game_quiz();
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// query the table to check if relation already exists
		$query = "SELECT * FROM `".$prefix."game_quiz` WHERE `game_id` = '".$game_id."' AND `quiz_id` = ".$quiz_id."";
		$result = $this->db_select($query);

		// if relation isnt registered yet, registers the player
		if(!$result) {
			$query = "INSERT INTO `".$prefix."game_quiz` (`game_id`,`quiz_id`)
						VALUES (".$game_id.", '".$quiz_id."')";
			$result = $this->db_query($query);
			
			// error
			if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_register_game_quiz())<br>Error? '.$error;
				return false;
			}
		}
	}


	// receives user game type name and returns its id
	public function db_get_game_typeID($type_name)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		// selects id(game_type_id), from a {type_name}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$game_type_id = $this->db_select("SELECT id
			FROM `".$prefix."game_type` 
			WHERE `name` = '".$type_name."'");
		// error
		if($game_type_id === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_game_typeID()) Error? '.$error);
			return false;
		}
		return $game_type_id[0]['id'];
	}


	// receives game id and coourse id, then relation id
	public function db_get_game_courseID($game_id,$course_id)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		// selects id(playerid), from a {userid && courseid}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$game_course_id = $this->db_select("SELECT id
			FROM `".$prefix."game_course` 
			WHERE `game_id` = '".$game_id."'
			and `course_id` = '".$course_id."'");
		// error
		if($game_course_id === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (game_type()) Error? '.$error);
			return false;
		}
		return $game_course_id[0]['id'];
	}

	// receives user game name and returns its id
	public function db_get_gameID($game_name)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		// selects id(game_id), from a {game_name}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$game_course_id = $this->db_select("SELECT id
			FROM `".$prefix."game` 
			WHERE `name` = '".$game_name."'");
		// error
		if($game_course_id === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_gameID()) Error? '.$error);
			return false;
		}
		return $game_course_id[0]['id'];
	}


	// receives game type id and returns all games of type created
	public function db_get_game_of_type($game_typeID)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the games class (once, avoiding redeclaration)
		require_once 'obj/current_game.php';
		
		// selects informations of game, from a {game_type_id}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_games = $this->db_select("SELECT id,game_type_id,name
			FROM `".$prefix."game` 
			WHERE `game_type_id` = '".$game_typeID."'");
		// error
		if($rows_games === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_game_of_type()) Error? '.$error);
			return false;
		}

		$games = array();

		// loop through games registered in the database to store in the array
		foreach($rows_games as $rows_game)
		{
			// creates a new game that
			// will be added to the array of games
			$game = new current_game();
			// stores known informations
			$game->name = $rows_game['name'];
			$game->gameid = $rows_game['id'];
			$game->gametypeid = $rows_game['game_type_id'];
			
			// adds the game, with all its information, to the array of games
			$games[] = $game;
		}
		return $games;
	}

	// receives game type id and course_id, it returns game list on the course
	public function db_get_game_of_course($game_typeID,$courseID)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the games class (once, avoiding redeclaration)
		require_once 'obj/current_game.php';
		
		// selects game informations(game course), from a {game_typeID && courseID}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_games = $this->db_select("SELECT `g`.`id`,`g`.`game_type_id`,`g`.`name`,`gc`.`id` as `game_course_id`
			FROM `".$prefix."game` as `g`
			JOIN `".$prefix."game_course` as `gc` on `gc`.`game_id`=`g`.`id`
			WHERE `g`.`game_type_id` = '".$game_typeID."'
			AND `gc`.`course_id` = ".$courseID."
			AND `gc`.`deleted`=0");
		// error
		if($rows_games === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_game_of_course()) Error? '.$error);
			return false;
		}

		$games = array();
		
		// loop through games registered in the database to store in the array
		foreach($rows_games as $rows_game)
		{
			// creates a new game that
			// will be added to the array of games
			$game = new current_game();
			// stores known informations
			$game->name = $rows_game['name'];
			$game->gameid = $rows_game['id'];
			$game->gametypeid = $rows_game['game_type_id'];
			$game->gamecourseid = $rows_game['game_course_id'];
			// adds the game, with all its information, to the array of games
			$games[] = $game;
		}
		return $games;
	}


	// receives game type id and course_id, it returns game list that is not associate to the course
	public function db_get_game_not_course($game_typeID,$courseID)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the games class (once, avoiding redeclaration)
		require_once 'obj/current_game.php';
		
		// selects game informations(game course), from a {game_typeID && courseID}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_games = $this->db_select("SELECT `g`.`id`,`g`.`game_type_id`,`g`.`name`,`gc`.`id` as `game_course_id`
			FROM `".$prefix."game` as `g`
			JOIN `".$prefix."game_course` as `gc` on `gc`.`game_id`=`g`.`id`
			WHERE `g`.`game_type_id` = '".$game_typeID."'
			AND `gc`.`course_id` <> ".$courseID."
			AND `gc`.`deleted`=0");
		// error
		if($rows_games === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_game_of_course()) Error? '.$error);
			return false;
		}

		$games = array();
		
		// loop through games registered in the database to store in the array
		foreach($rows_games as $rows_game)
		{
			// creates a new game that
			// will be added to the array of games
			$game = new current_game();
			// stores known informations
			$game->name = $rows_game['name'];
			$game->gameid = $rows_game['id'];
			$game->gametypeid = $rows_game['game_type_id'];
			$game->gamecourseid = $rows_game['game_course_id'];
			// adds the game, with all its information, to the array of games
			$games[] = $game;
		}
		return $games;
	}

	//receives course id and returns all quiz at course context
	public function db_get_quiz_of_course($courseID)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the quiz class (once, avoiding redeclaration)
		require_once 'obj/quiz.php';
		
		// selects quiz informations (quiz), from a {courseid}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_quiz = $this->db_select("SELECT `q`.`id`,`q`.`name`
			FROM `".$prefix."quiz` as `q`
			WHERE `q`.`course` = '".$courseID."'");
		// error
		if($rows_quiz === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_quiz_of_course()) Error? '.$error);
			return false;
		}

		$quiz = array();
		
		// loop through quiz registered in the database to store in the array
		foreach($rows_quiz as $row)
		{
			// creates a new quiz that
			// will be added to the array of quizess
			$current_quiz = new quiz();
			// stores known informations
			$current_quiz->name = $row['name'];
			$current_quiz->id = $row['id'];
			
			// adds the quiz, with all its information, to the array of quizess
			$quiz[] = $current_quiz;
		}
		return $quiz;
	}

	//receives game_course id and returns all questions id
	public function db_get_questions_of_game($gamecourseID)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// selects quiz informations (quiz), from a {courseid}
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_questions = $this->db_select("SELECT DISTINCT `qs`.`questionid`
			FROM  `".$prefix."game_course` as `gc`
			JOIN `".$prefix."game_quiz` as `gq` on `gc`.`game_id`=`gq`.`game_id`
			JOIN `".$prefix."quiz` as `q` on `q`.`id` = `gq`.`quiz_id`
			JOIN `".$prefix."quiz_slots` as `qs` on `qs`.`quizid` = `q`.`id`
			WHERE `gc`.`id` = ".$gamecourseID."");
		// error
		if($rows_questions === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			error_log('error: could not query the database (db_get_questions_of_game()) Error? '.$error);
			return false;
		}

		$questions = array();
		
		// loop through quiz registered in the database to store in the array
		foreach($rows_questions as $question)
		{
			$questions = array_push($question['questionid']);
		}

		return $rows_questions;
	}

	//receives game course id and returns list of players
	public function db_get_player_info($game_course_id)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the players class (once, avoiding redeclaration)
		require_once 'obj/player.php';
		
		// the array of players that will be returned by this function
		$players = array();
		
		// selects player informations from all players in game course
		// to store in the array of players
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_players = $this->db_select("SELECT `gp`.`id`, `gp`.`user_id`,`gp`.`game_course_id`,concat(`u`.`firstname`,' ',`u`.`lastname`) as `name`,`gp`.`score`
			FROM `".$prefix."game_player` as `gp`
			JOIN `".$prefix."user` as `u` on `u`.`id`=`gp`.`user_id`
			WHERE `gp`.`game_course_id` = ".$game_course_id."
			order by `gp`.`score` desc");
		// error
		if($rows_players === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_player_info())<br>Error? '.$error;
			return false;
		}
		
		// loop through players registered in the database to store in the array
		foreach($rows_players as $rows_player)
		{
			// creates a new player that
			// will be added to the array of players
			$player = new player();
			// stores known informations
			$player->playerid = $rows_player['id'];
			$player->userid = $rows_player['user_id'];
			$player->gameid = $rows_player['game_course_id'];
			$player->name = $rows_player['name'];
			$player->score = $rows_player['score'];
			
			// adds the player, with all its information, to the array of players
			$players[] = $player;
		}
		
		// now we have all the info needed. return the array
		return $players;
	}


	//receives game course id and returns list of players
	public function db_get_current_player_info($playerid)
	{
		// Connect to the database
		$connection = $this->db_connect();

		// imports the players class (once, avoiding redeclaration)
		require_once 'obj/player.php';
		
		// the array of players that will be returned by this function
		$players = array();
		
		// selects player informations from all players in game course
		// to store in the array of players
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		$rows_players = $this->db_select("SELECT `gp`.`id`, `gp`.`user_id`,`gp`.`game_course_id`,concat(`u`.`firstname`,' ',`u`.`lastname`) as `name`,`gp`.`score`
			FROM `".$prefix."game_player` as `gp`
			JOIN `".$prefix."user` as `u` on `u`.`id`=`gp`.`user_id`
			WHERE `gp`.`id` = ".$playerid."");
		// error
		if($rows_players === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (db_get_player_info())<br>Error? '.$error;
			return false;
		}
		
		// loop through players registered in the database to store in the array
		foreach($rows_players as $rows_player)
		{
			// creates a new player that
			// will be added to the array of players
			$player = new player();
			// stores known informations
			$player->playerid = $rows_player['id'];
			$player->userid = $rows_player['user_id'];
			$player->gameid = $rows_player['game_course_id'];
			$player->name = $rows_player['name'];
			$player->score = $rows_player['score'];
			
			// adds the player, with all its information, to the array of players
			$players[] = $player;
		}
		
		// now we have all the info needed. return the array
		return $players;
	}

	//this function receibe a game course id and removes it at plugin
	public function db_remove_game_course($game_course_id){
		// Connect to the database
		$connection = $this->db_connect();
		
		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// update status of game course to delete
		$query = "UPDATE `".$prefix."game_course` SET `deleted`=1 WHERE `id` = '".$game_course_id."'";
		$result = $this->db_query($query);
		if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_remove_game_course())<br>Error? '.$error;
				return false;
		}
	}


	//this function receive a player id and set new score
	public function db_update_player($playerid,$score){
		// Connect to the database
		$connection = $this->db_connect();

		
		// gets the prefix of moodle database tables
		$config = parse_ini_file('db/dbconfig.ini'); 
		$prefix = $config['prefix'];
		
		// update player
		$query = "UPDATE `".$prefix."game_player` SET `score`='" . $score . "' WHERE `id` = '".$playerid."'";
		$result = $this->db_query($query);
		if($result === false) {
				$error = $this->db_error();
				// Handle error - inform administrator, log to file, show error page, etc.
				echo 'error: could not query the database (db_update_player())<br>Error? '.$error;
				return false;
		}
	}		
}

	