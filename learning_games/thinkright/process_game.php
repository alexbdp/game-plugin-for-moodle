<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$game = $_GET['game'];

// get player id in current game
$player_id = $db->db_get_playerID($userid,$game);

// verify if user exists, if doesnt, create it
if ($player_id == false){
	$db->register_player($userid,$game);
	$player_id = $db->db_get_playerID($userid,$game);
}

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$path_games = __DIR__ ;

// path to the description file of the game, containing all
// info that we`ll be loading on our data structure
$description_file = $path_games . '/description.php';

// include the game description file to read its vars
include $description_file;

// stores the remaining info of the game on the data structure

require_once 'obj/question_type.php';

$questionsid = $db->get_questionsID($game);

$qst_size = count($questionsid);

echo('
<!DOCTYPE html>
<html>
	<head>
		<title>Think Right</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
	</head>
	<body>
	<div class="container">
	<div class = "header">
		<a href="thinkright.php"><img id="logo" aling="center" src="images/logo.png" width="125px" height="50px" left="0px" position="relative"></img></a>
		<strong><a href="#"><span class="instructions">INSTRUCTIONS</span></a>
		<a href="'. $plugin_path . '"><strong><p id="exit">EXIT</p></a>
	</div>
	<div id="questions_box">
	<ul>');

	$i=1;

	
	
	foreach ($questionsid as $qid) {
		if ($i <= 36){
			echo ('<li><button value="'. $qid->id .',' . $qid->type . ',' . $player_id . '" class="coin">' . $i .'</button></li>');
			$i++;
		}
	};


$i = $i -1;

echo('
	</ul>
	</div>
	<div class="progress"><div id="singleplayer"></div></div>
	<div class="score">
		<center>
		<div><h1 id="npoints">00</h1></div>
		<p>SCORE</p>
		<div><h1 id="nquestions">00</h1></div>
		<p>QUESTIONS</p>
		</center>
	</div>
	<div class="content"></div>
	<div class="box_instructions">
		<a class="leave"><button>X</button></a>
		<h1>Intructions</h1>
		<h3>Objective</h3>
		<p>Answer correctly 50% of questions of the game</p>
		<h3>Points</h3>
		<ul>
		<li><strong>True or False </strong>50</li>
		<li><strong>Singlechoice: </strong>70</li>
		<li><strong>Multichoice: </strong>100 if all choices are right and punishment of 10 points for each choice wrong</li>
		</ul>
		<h3>Bonus</h3>
		<p>If you answer right all questions, you get more 100 points</p>
		<h3>Punishment</h3>
		<p>If you answer wrong some question, you loose 10 points</p>
	</div>
	<div id="lost"><center><p><span class="lost">
	<font size="35">You lost this game!</font></span></p> 
				<img src="images/lost.png" width="150px" height="150px" margin-footer="10px"></img></center>
	
	<center><form action="process_game.php" method="GET" class="game_list">
	<button class="menu_button" name="game" type="submit" value="' . $game . '">Try Again</button>
	</form></center>
	<a href="thinkright.php">
	<center><p class = "menu_button">Home</p>
	</center></a></div>
	<button id="question" value="' . $i . '"></button>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/start.js"></script>
    <script src="js/question_mode.js"></script>
	</body>
<html>
');
?>