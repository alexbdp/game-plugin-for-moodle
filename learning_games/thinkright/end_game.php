<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

$tags = parse_ini_file(__DIR__ . "/../../config.ini"); 
$root = $tags['root'];

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$path_games = __DIR__ ;

echo('<div>
	<h1>Congratulations,</h1>
<h1>You Won!</h1>');

$data = $_POST['data'];

$array = explode(',', $data);

$playerid = $array[3];
$score = $array[1];
$qtd_qst = $array[0];
$qtd_ans = $array[4];


$bonus = 0;

if ($qtd_qst == $qtd_ans){
	$bonus = 100;
}

$score = $score + $bonus;

// update player with $score
$db->update_player($playerid,$score);

echo('<center><h3>Bonus:' . $bonus . '</h3></center>
	<h2>Your Score: ' . $score . '</h2>

<center><img src="images/victory.png" width="143" height="200"></img>
<a href="thinkright.php"><center><p class = "home">Home</p></center></a>');

if ($qtd_qst > 0){
	echo ('<p>If you want, you can continue this game</p>
		<center><p class = "continue">Continue</p></center>');
}

echo ('<div>');