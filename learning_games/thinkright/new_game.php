<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);


$path_games = __DIR__ ;

// path to the description file of the game, containing all
// info that we`ll be loading on our data structure
$description_file = $path_games . '/description.php';

// include the game description file to read its vars
include $description_file;

// stores the remaining info of the game on the data structure
$type = $name;

// get id from current type game
$game_typeID = $db->get_game_typeID($type);


// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

// get capability of user
$cap = $db->get_user_capability();

echo('
<!DOCTYPE html>
<html>
	<head>
		<title>Think Right</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
		<script src="js/jquery1_11_13.js" type="text/javascript"></script>
	</head>
	<body>
	<div class="container">');



// verify if user can create game
if ($cap == "manage"){
	echo ('<a href="thinkright.php"><img id="logo" aling="center" src="images/logo.png" width="250px" height="100px"></img></a>
		<h1 class="remove_title">How Do You Want to Create Your Game?</h1>
		<center><center><p class = "associate_button">Associate Game</p></center>
		<center><p class = "new_button">New</p></center>
			<div class="new_game"><center><div class="options_box">
			<form action="process_new_game.php" method="GET">');

	$i = 0;

	echo ('<div height="70px"><p><span id="game_name">Game Name: <input type="text" name="name" /></span></p>
				 <strong><p>Select which quizes bellow you want to associate to your game:</p></strong></div>');

	// get class of quiz
	require_once $root . '/blocks/games/obj/quiz.php';

	// get all quiz registered on bd for current course id
	$quizes = $db->get_quiz_of_course($courseid);

	// list all quiz registered to associate to a new game
	foreach($quizes as $quiz){
		$quiz_name = htmlentities($quiz->name ,  ENT_COMPAT,'ISO-8859-1', true);
		if (($i%2) == 0){
			echo ('<div class="option_one"><input type="checkbox" class="checkbox" value="' . $quiz->id . '" name="quiz_choice[]" /><span class="line_name">' . $quiz_name .'</span></div>');
		}
		else {
			echo ('<div class="option_two"><input type="checkbox" class="checkbox" value="' . $quiz->id . '" name="quiz_choice[]" /><span class="line_name">' . $quiz_name .'</span></div>');	
		}
		$i++;
	};

	echo('		</div><input type="submit" class="submit" value="Create" name="" /></div>
			</form></center>');


	// list all games to associate to game
	echo ('<div class="associate_game"><center><div class="options_box">');

	echo ('<div height="70px"><strong><p>Select the game that you want to associate to your course</p></strong></div>');

	// list all quiz registered to associate to a new game
	echo ('	<form action="associate_game.php" method="GET">');
		
		require_once $root . '/blocks/games/obj/current_game.php';

		$game_course = $db->get_game_not_course($game_typeID,$courseid);

		$i=0;
		foreach($game_course as $game)
		{
			if (($i%2) == 0){
				echo ('<div class="option_one"><span><input type="radio" value="' . $game->gameid . '" name="game[]" /><span class="line_name">' . $game->name .'</span></span></div>');
			}
			else {
				echo ('<div class="option_two"><span><input type="radio" value="' . $game->gameid . '" name="game[]" /><span class="line_name">' . $game->name .'</span></span></div>');	
			}
			$i++;
		};

		echo('</div></center>
			<center><input type="submit" class="submit" value="Create" name="" /></center>
			</form>');
}


else {
	echo ('<h1>You Dont Have Permission For This Session!</h1>');
}

echo('		<p><a href="'. $plugin_path . '"><strong><p id="exit">EXIT</p></strong><img id="logout" aling="center" src="images/logout.png" width="70px" height="70px"></img></a>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/start.js"></script>
	</body>
<html>
');

?>