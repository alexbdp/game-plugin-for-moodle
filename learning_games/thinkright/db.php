<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

// db config
$config = parse_ini_file('../../db/dbconfig.ini'); 
// db helper include
// path to the db helper file
$tags = parse_ini_file(__DIR__ . "/../../config.ini"); 
$root = $tags['root'];
$dbhelper_file = $root . '/blocks/games/dbhelper.php';
$multichoice_file = $root . '/blocks/games/obj/multichoice_question.php';
$path_games = __DIR__ ;

require_once ($dbhelper_file);

// imports the multichoice question class (once, avoiding redeclaration)
require_once ($multichoice_file);



// class responsable for handling db methods and operations (quiz game)
class db {
	
	// The database connection 
    protected $connection;
	// dbhelper object
	protected $dbhelper;

	// sets the dpHelper (initializes it)
	// if isn`t set
	function set_dbhelper()
	{
		if(!isset($this->dbhelper))
			$this->dbhelper = new dbHelper();
	}
	
	/**
     * Connect to the database provided in dbconfig.ini
     * 
     * @return bool false on failure / mysqli MySQLi object instance on success
     */
	public function db_connect()
	{
		$this->set_dbhelper();
		
		// Try and connect to the database
        if(!isset($this->connection)) {
            // Load configuration as an array. Use the actual location of your configuration file
            $config = $GLOBALS['config']; 
            $this->connection = new mysqli($config['host'],$config['username'],$config['password'],$config['dbname']);
        }

        // If connection was not successful, handle the error
        if($this->connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
			echo 'Could not connect to the database. Check your database config file.';
            return  mysqli_connect_error(); 
        }
        return $this->connection;
	}
	
	/**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function db_query($query) {
        // Connect to the database
        $connection = $this->db_connect();

        // Query the database
        $result = $connection -> query($query);

        return $result;
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function db_select($query) {
        $rows = array();
        $result = $this -> db_query($query);
        if($result === false) {
            return false;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function db_error() {
        $connection = $this -> db_connect();
        return $connection -> error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function db_quote($value) {
        $connection = $this -> db_connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }
	
	/** DP HELPER PLUGIN EXISTING METHODS CALLS **/
	
	// returns the player id of a respective
	// user id and course id (a plugin player)
	public function db_get_playerID($userid, $courseid) 
	{
		$this->set_dbhelper();
		return $this->dbhelper->db_get_playerID($userid, $courseid);
	}
	
	// returns an array of players of a course
	// with all info needed for the games 
	// require the class players in /obj
	public function db_get_players($courseid)
	{
		$this->set_dbhelper();
		return $this->dbhelper->db_get_player_info($courseid);
	}
	
	// returns an array of multichoice questions 
	// with all info needed for the games
	// require the class of multi choice questions
	public function get_multichoice_questions($category)
	{
		$this->set_dbhelper();
		return $this->dbhelper->get_multichoice_questions($category);
	}
	
	// returns the list of categories in the question bank
	public function db_get_categories()  
	{
		$this->set_dbhelper();
		return $this->dbhelper->db_get_categories();
	}
	
	// receives user id and returns user name
	public function db_get_user_name($userid)
	{
		$this->set_dbhelper();
		return $this->dbhelper->db_get_user_name($userid);
	}
	
	// receives user player id and returns user name
	public function db_get_player_name($playerid)
	{
		$this->set_dbhelper();
		return $this->dbhelper->db_get_player_name($playerid);
	}
	
	// receives course id and returns course name
	public function db_get_course_name($courseid)
	{
		$this->set_dbhelper();
		return $this->dbhelper->db_get_course_name($courseid);
	}
	
	// returns the id, on the db, of the current logged user 
	public function get_userID()
	{
		$this->set_dbhelper();
		return $this->dbhelper->get_userID();
	}
	
	// returns the id, on the db, of the current course user is in
	public function get_courseID()
	{
		$this->set_dbhelper();
		return $this->dbhelper->get_courseID();
	}
	
	// gets current user capability
	public function get_user_capability()
	{
		$this->set_dbhelper();
		return $this->dbhelper->get_user_capability();
	}

	public function get_game_of_course($game_id,$course_id)
	{
		$this->set_dbhelper;
		return $this->dbhelper->db_get_game_of_course($game_id,$course_id);
	}

	public function get_game_not_course($game_id,$course_id)
	{
		$this->set_dbhelper;
		return $this->dbhelper->db_get_game_not_course($game_id,$course_id);
	}

	function get_game_typeID ($type_name){
		$this->set_dbhelper();
		return $this->dbhelper->db_get_game_typeID($type_name);
	}

	function get_questionsID ($gamecourseID){
		// Connect to the database
		$connection = $this->db_connect();
		
		$config = $GLOBALS['config']; 
		// gets the prefix of moodle database tables
		$prefix = $config['prefix'];
		
		$rows_questions = $this->db_select("SELECT DISTINCT `qs`.`questionid`,`qst`.`qtype`
			FROM  `".$prefix."game_course` as `gc`
			JOIN `".$prefix."game_quiz` as `gq` on `gc`.`game_id`=`gq`.`game_id`
			JOIN `".$prefix."quiz` as `q` on `q`.`id` = `gq`.`quiz_id`
			JOIN `".$prefix."quiz_slots` as `qs` on `qs`.`quizid` = `q`.`id`
			JOIN `".$prefix."question` as `qst` on `qst`.`id` = `qs`.`questionid`
			WHERE `gc`.`id` = ".$gamecourseID."
			AND `qst`.`qtype` in ('multichoice','truefalse')");
		// error
		if($rows_questions === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (get_multichoice_question())<br>Error? '.$error;
			return false;
		}

		require_once 'obj/question_type.php';

		$questions = array();
		
		// loop through quiz registered in the database to store in the array
		foreach($rows_questions as $row_question)
		{
			$question = new question_type;
			$question->id = $row_question['questionid'];
			$question->type = $row_question['qtype'];
			$questions[] = $question;
		}

		return $questions;
	}


	function register_game($typeid, $name_game){
		$this->set_dbhelper();
		$this->dbhelper->db_register_game($typeid, $name_game);
	}


	function register_game_course($gameid,$courseid){
		$this->set_dbhelper();
		$this->dbhelper->db_register_game_course($gameid,$courseid);
	}

	function register_game_quiz($quiz_id,$game_id){
		$this->set_dbhelper();
		$this->dbhelper->db_register_game_quiz($quiz_id,$game_id);
	}

	function register_player($userid, $courseid){
		$this->set_dbhelper();
		$this->dbhelper->db_register_player($userid,$courseid);
	}	

	function get_gameID($game_name){
		$this->set_dbhelper();
		return $this->dbhelper->db_get_gameID($game_name);
	}

	function get_quiz_of_course($course_id){
		$this->set_dbhelper();
		return $this->dbhelper->db_get_quiz_of_course($course_id);
	}



	function get_player_info($playerid){
		$this->set_dbhelper();
		return $this->dbhelper->db_get_current_player_info($playerid);
	}

	function remove_game_course($gamecourseid){
		$this->set_dbhelper();
		return $this->dbhelper->db_remove_game_course($gamecourseid);
	}


	function update_player($playerid,$score){
		$this->set_dbhelper();
		return $this->dbhelper->db_update_player($playerid,$score);
	}

	// gets all informations necessary from a specific question (receives the question id)
	public function get_multichoice_question($questionid)
	{
		// Connect to the database
		$connection = $this->db_connect();
		
		$config = $GLOBALS['config']; 
		// gets the prefix of moodle database tables
		$prefix = $config['prefix'];
		
		$row_question = $this->db_select("SELECT `id`, `name`,`questiontext` 
												FROM `".$prefix."question` 
												WHERE `id` = ".$questionid."");
		// error
		if($row_question === false) {
			$error = $this->db_error();
			// Handle error - inform administrator, log to file, show error page, etc.
			echo 'error: could not query the database (get_multichoice_question())<br>Error? '.$error;
			return false;
		}

		// creates a new multichoice question that
		// will be added to the array of mc questions
		$mc_question = new multichoice_question();

		// stores known informations
		$mc_question->name = $row_question[0]['name'];
		$mc_question->text = strip_tags($row_question[0]['questiontext']);

		// now that we have the question id
		// we will look for its answers and store it
		$mc_question->answers = array();
		$mc_question->correct_answers_idx = array();
		

		$rows_answers = $this->db_select("SELECT `id`,`answer`,`fraction` FROM `".$prefix."question_answers` WHERE `question` = ".$questionid."");
		
		// counter of correct answers
		$i = 0;
		// loop through each multichoice questions answers found in the bank
		foreach($rows_answers as $row_answer)
		{
			// adds answer to respective positions in the array of answers (starting from 0)
			$mc_question->answers[($row_answer['id'] - 1)%(count($rows_answers))] = strip_tags($row_answer['answer']);
			// if answer is the correct answer, stores the correct id of the array of answers in the correct answers array
			if(($row_answer['fraction'] - 1.0) > - 1.0)
			{
				$mc_question->correct_answers_idx[$i] = ($row_answer['id'] - 1)%(count($rows_answers));
				$i++;
			}
		}
		// adds the question, with all its information
		return $mc_question;

	}

}