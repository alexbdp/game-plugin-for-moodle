<?php

$name = 'Think Right';  // The game name
$description = 'Can you get highest score in this game?'; // A short description of the game
$category = 'board'; // the category of the game (board, quiz, rpg, action, other)
$author   = 'Alex'; // The author of the game
$file_name = 'thinkright.php' // The name of the main game file

// logo will be automatically loaded from the game directory root, as logo.gif
?>