<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

$tags = parse_ini_file(__DIR__ . "/../../config.ini"); 
$root = $tags['root'];

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$path_games = __DIR__ ;

echo('<div>');


require_once $root . '/blocks/games/obj/multichoice_question.php';

$data = $_POST['data'];

$array = explode(',', $data);

$player = $array[2];

$questionid = $array[0];



// verify type of question
if ($array[1] == 'multichoice' || $array[1] == 'truefalse'){

	// get question information
	$question = $db->get_multichoice_question($questionid);


	// print text of question
	$qst_text = htmlentities($question->text,  ENT_COMPAT,'ISO-8859-1', true);

	echo('<p class="enunciation">' . $qst_text . '</p>');

	$right_answer = count($question->correct_answers_idx);

	// process question of type single choice or true and false
	if ($right_answer === 1) {

		echo ('<form method="POST" action="" id="multichoice">');

		for ($i=0;$i<count($question->answers);$i++){
			$id[] = $i;
		}

		shuffle($id);

		$i = 0;

		while ($i < count($question->answers) ){
			$idr = $question->correct_answers_idx[0];
			$qst_option  = htmlentities($question->answers[$id[$i]],  ENT_COMPAT,'ISO-8859-1', true);
			$question_answer = htmlentities($question->answers[$idr],  ENT_COMPAT,'ISO-8859-1', true);
			// send for process answer: playerid, type of question, number of right questions, id current and id of question right
			echo '<input type="radio" class="checkbox" value="' . $player . ',' . $array[1] . ','  . $right_answer . ','. $id[$i] . ',' . $idr . ',' . $question_answer .',' . $questionid .'" name="radio[]" />' . $qst_option . '<br />';
			$i++;
		}


		echo ('<center><input type="submit" class="submit" value="OK" name="" /></center>
			</form></div>');
	}

	elseif ($right_answer != 1) {

		echo ('<form method="POST" action="" id="multichoice">');

		for ($i=0;$i<count($question->answers);$i++){
			$id[] = $i;
		}

		shuffle($id);

		$i = 0;

		$nquestions = count($question->answers);

		while ($i < count($question->answers) ){
			$qst_option  = htmlentities($question->answers[$id[$i]],  ENT_COMPAT,'ISO-8859-1', true);
			// send for process answer: playerid, type of question, number of right questions, id current and ids of answers right
			echo ('<input type="checkbox" class="checkbox" value="' . $player . ',' . $array[1] . ','  . $right_answer . ','. $nquestions . ',' . $id[$i]);
			for ($y=0;$y<count($question->correct_answers_idx);$y++){
				$idr = $question->correct_answers_idx[$y];
				$question_answer = htmlentities($question->answers[$idr],  ENT_COMPAT,'ISO-8859-1', true);
				echo (',' . $idr . ',' . $question_answer);
			}
			echo ',' . $questionid . '" name="checkbox[]" />' . $qst_option . '<br />';
			$i++;
		}


		echo ('<p><center><input type="submit" class="submit" value="OK" name="" /></center>
			</form></div>');
	}
}


?>