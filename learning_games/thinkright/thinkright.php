<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

// verify if user can manager
$cap = $db->get_user_capability();

echo('
<!DOCTYPE html>
<html>
	<head>
		<title>Think Right</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
	</head>
	<body>
	<div class="container">
	<a href="thinkright.php"><img id="logo" aling="center" src="images/logo.png" width="250px" height="100px"></img></a>
	<h1> Hello '.$username.'!</h1>');

if ($cap == "manage")
{
		echo ('<h2> What Do You Wanna Do? </h2>');
}
echo ('<center><div><a href="game_list.php"><center><p class = "menu_button">Play</p></center></a></div></center>');



// only editing teachers and managers can see cog icon (options menu)
if ($cap == "manage")
{
	echo('		<center><a href="new_game.php"><center><p class = "menu_button">New Game</p></center></a>
				<center><a href="remove_games.php"><center><p class = "menu_button">Remove Game</p></center></a>'

		);
}
		
echo('<a href="'. $plugin_path . '"><strong><p id="exit">EXIT</p></strong><img id="logout" aling="center" src="images/logout.png" width="70px" height="70px"></img></a>
	</body>
<html>
');
?>