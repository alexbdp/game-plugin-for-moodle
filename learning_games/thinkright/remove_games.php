<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$cap = $db->get_user_capability();

$path_games = __DIR__ ;

// path to the description file of the game, containing all
// info that we`ll be loading on our data structure
$description_file = $path_games . '/description.php';

// include the game description file to read its vars
include $description_file;

// stores the remaining info of the game on the data structure
$type = $name;

// get id from current type game
$game_typeID = $db->get_game_typeID($type);

echo('
<!DOCTYPE html>
<html>
	<head>
		<title>Think Right</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
	</head>
	<body>
	<div class="container">
	<a href="thinkright.php"><img id="logo" aling="center" src="images/logo.png" width="250px" height="100px"></img></a>
	');


// verify if user can manager game
if ($cap == "manage"){
	require_once $root . '/blocks/games/obj/current_game.php';

	$game_course = $db->get_game_of_course($game_typeID,$courseid);

	if (empty($game_course)){
		echo ('<h1>There is no game created</h1>');
	}
	else {
		echo ('<center><div class="options_box">
			<h2>Games Created:</h2>
			<form action="process_remove_game.php" method="GET">');
		$i=0;
		foreach($game_course as $game)
		{
			if (($i%2) == 0){
				echo ('<div class="option_one"><span><input type="checkbox" value="' . $game->gamecourseid . '" name="game_course[]" /><span class="line_name">' . $game->name .'</span></span></div>');
			}
			else {
				echo ('<div class="option_two"><span><input type="checkbox" value="' . $game->gamecourseid . '" name="game_course[]" /><span class="line_name">' . $game->name .'</span></span></div>');	
			}
			$i++;
		};

		echo('</div></center>
			<center><input type="submit" class="submit" value="Remove" name="" /></center>
			</form>');
	}

}
else {
	echo ('<h1>You Dont Have Permission For This Session!</h1>');
}


echo('<a href="'. $plugin_path . '"><strong><p id="exit">EXIT</p></strong><img id="logout" aling="center" src="images/logout.png" width="70px" height="70px"></img></a>
	</div>
	</body>
<html>
');
?>