var main = function() {


// action to start a game
  $(".content").hide();

  $('.coin').addClass('qst_enable');
  
  $("#lost").hide();

  $("#win").hide();

  var qst = $('#question').val();

  $('#question').hide();

  if (qst >= 17){
  	$('.coin').addClass('small');
  }
  if (qst < 17 && qst >= 10){
  	$('.coin').addClass('medium');
  }
  if (qst < 10){
  	$('.coin').addClass('large');
  }

// action on remove_game

  $('.new_button').click(function() {
    $('.new_game').show();
    $('.associate_button').hide();
    $('.new_button').hide();
    $('.remove_title').hide();
  });

  $('.associate_button').click(function() {
    $('.associate_game').show();
    $('.associate_button').hide();
    $('.new_button').hide();
    $('.remove_title').hide();

  });


}

$(document).ready(main)