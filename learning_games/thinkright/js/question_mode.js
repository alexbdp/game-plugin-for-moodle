var main = function() {


  //get number of questions in this game
  var qst = $('#question').val();

  // number of questions right to win this game
  var min = qst/2;
  var min = Math.ceil(min);

  // number of questions and points of player
  var nqst = 0;
  var score = 0;

  //get initial position of player
  var position = $('#singleplayer').offset();
  var y = (position.top);
  var new_position = y;
  var progress = y/min;

  //verify if continue
  var i = 0;
  
  //if answer wrong, there's one less question
  $(document).on('click','.wrong',function(){
    qst = qst - 1;
    score = score - 10;
    var num = $(this).val();
    $('#npoints').text(score);
    $('.content').hide();
    if (qst < 1 && nqst < min){
      $('#lost').show();
    }
    else if (qst < 1 && nqst >= min){
      $.ajax({
        type: "POST",
        url: "./end_game.php",
        data: 'data=' + qst + ',' + score + ',' + num + ',' + nqst,
        async: true,
        success: function(result)
        {
          $('.content').show();
          $(".content" ).html(result);
        }
      });
    }
  });


  //if answer right, there's one less question and player move
  $(document).on('click','.right',function(){
    $('.content').hide();
    if (i==0){
        new_position = new_position - progress;
        $('#singleplayer').css("top", new_position + 'px');
    }
    nqst = nqst + 1;
    qst = qst - 1;
    var num = $(this).val();
    var b = num.indexOf(',');
    var a = num.slice(0,b);
    score = score + parseInt(a);
    $('#nquestions').text(nqst);
    $('#npoints').text(score);
    if (nqst == min && i == 0){
      i = i + 1;
      $.ajax({
        type: "POST",
        url: "./end_game.php",
        data: 'data=' + qst + ',' + score + ',' + num + ',' + nqst,
        async: true,
        success: function(result)
        { $('.content').height(555);
          $('.content').show();
          $(".content" ).html(result);
        }
      });
    }
    else if (nqst < min && qst == 0){
          $('#lost').show();
    }
    else if (qst == 0 && i == 1 && nqst >=min){
      $.ajax({
        type: "POST",
        url: "./end_game.php",
        data: 'data=' + qst + ',' + score + ',' + num + ',' + nqst,
        async: true,
        success: function(result)
        { $('.content').height(550);
          $('.content').show();
          $(".content" ).html(result);
        }
      });
    }
  });


  $('.qst_enable').click(function() {
    var data = $(this).val();
    $.ajax({url: './process_question.php',
      type: 'POST',
      data: 'data=' + data,
      async: true,
      success: function(result){
        $('.content').height(500);
        $('.content').show();
        $( ".content" ).html(result);
    }});
    $(this).removeClass('qst_enable');
    $(this).addClass('qst_disabled');
    $(this).attr('disabled', 'disabled');
  });

  $('.instructions').click(function() {
    $('.box_instructions').show();
  });



  $(document).on('click','.empty',function() {
    $('.content').hide();
  });

  $(document).on('click','.continue',function() {
    $('.content').hide();
  });


  $('.leave').click(function() {
    $('.box_instructions').hide();
  });

  $(document).on('click','.submit',function(){
    $('#multichoice').submit(function(){
      var data = $(this).serialize();
      $.ajax({
        type: "POST",
        url: "./process_answer.php",
        data: data,
        async: true,
        success: function(result)
        {
          $('.content').show();
          $(".content" ).html(result);
        }
      });
      return false;
    });
  });



}

$(document).ready(main);