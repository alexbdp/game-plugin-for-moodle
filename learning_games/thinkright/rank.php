<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$path_games = __DIR__ ;

// path to the description file of the game, containing all
// info that we`ll be loading on our data structure
$description_file = $path_games . '/description.php';

// include the game description file to read its vars
include $description_file;

$game = $_GET['game'];


// get player id in current game
$player_id = $db->db_get_playerID($userid,$game);


echo('
<!DOCTYPE html>
<html>
	<head>
		<title>Think Right</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
	</head>
	<body>
	<div class="container">
	<a href="thinkright.php"><img id="logo" aling="center" src="images/logo.png" width="250px" height="100px"></img></a>
	<h1>Ranking</h1><center><div class="rank_box">');

require_once $root . '/blocks/games/obj/player.php';


$players = $db->db_get_players($game);

foreach ($players as $player) {
	if ($player->playerid == $player_id){
		echo ('<div class="rank_current"><ul>
			<li><div class="rank_player"><strong>' . $player->name . '
			</div></strong></li>
			<li><div class="rank_score"><strong> Score: '  . $player->score . '
			</strong></div></li></ul></div>');

	}
	else {
		echo ('<div class="rank_line"><ul>
			<li><div class="rank_player"><strong>' . $player->name . '
			</div></strong></li>
			<li><div class="rank_score"><strong> Score: '  . $player->score . '
			</strong></div></li></ul></div>');
	}
}


echo('<div><center><a href="game_list.php"><center><p class = "menu_button">Back</p></center></a></center>
	<a href="'. $plugin_path . '"><strong><p id="exit">EXIT</p></strong><img id="logout" aling="center" src="images/logout.png" width="70px" height="70px"></img></a>
	</div>
	</body>
<html>
');
?>