<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;


// get name of the new game
$game_name = $_GET['name'];


// get quiz's id for associate to the new game
$game_quiz = $_GET['quiz_choice'];

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$path_games = __DIR__ ;

// path to the description file of the game, containing all
// info that we`ll be loading on our data structure
$description_file = $path_games . '/description.php';

// include the game description file to read its vars
include $description_file;

// stores the remaining info of the game on the data structure
$type = $name;

// get type id
$game_typeID = $db->get_game_typeID($type);

// create game of type id receive above
$db->register_game($game_typeID,$game_name);

// get id of the new game
$gameid = $db->get_gameID ($game_name);

// register this game on current course
$db->register_game_course($gameid,$courseid);


// for each quiz select, associate to the new game
foreach($game_quiz as $quiz) {
    $db->register_game_quiz($quiz,$gameid);
}


echo('
<!DOCTYPE html>
<html>
	<head>
		<title>Think Right</title>
		<link type="text/css" rel="stylesheet" href="style.css"/>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
	<div class="container">
	<a href="thinkright.php"><img id="logo" aling="center" src="images/logo.png" width="250px" height="100px"></img></a>
	<h1> Your game has been created as  ' . $game_name . '!</h1>
	<h2> What do you wanna do now?</h2>
	<center><a href="thinkright.php"><center><p class = "menu_button">Home</p></center></a>
	<center><a href="new_game.php"><center><p class = "menu_button">New Game</p></center></a>
	<a href="'. $plugin_path . '"><strong><p id="exit">EXIT</p></strong><img id="logout" aling="center" src="images/logout.png" width="70px" height="70px"></img></a>
	</div>
	</div>
	</body>
<html>
');


?>