<?php

// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

$db_file = 'db.php';
// include the db file 
include_once $db_file;
// new db class (db class = db quiz game methods + db plugin methods)
$db = new db();

$tags = parse_ini_file(__DIR__ . "/../../config.ini"); 
$root = $tags['root'];

// gets info to be displayed in this menu
$userid = $db->get_userID();
$username = $db->db_get_user_name($userid);
$courseid = $db->get_courseID();
$coursename = $db->db_get_course_name($courseid);

// path to games plugin menu
$plugin_path = $tags['wwwroot']. '/blocks/games/games.php?id=' . $courseid;

$path_games = __DIR__ ;

$data = $_POST['radio'];

$array = explode(',', $data[0]);

echo('<div class="answer">');


// process question of type multichoice or true or false
if ($array[1] == 'multichoice' || $array[1] == 'truefalse'){

	// process answer of type single choice or true aor false 
	if ($array[2] == 1){
		// if answer is right
		if ($array[3] == $array[4] && $array[3]!=''){
			echo ('<center><p><span class="right_answer"><font size="35">Your Answer Is Right!</font></span></p>
				<img src="images/right.png" width="150px" height="150px" margin-footer="10px"></img>');

			if ($array[1] == 'multichoice' ){
				echo ('<center><p>Points: 70</p></center>
					<br><button class="right" value="70');
			}
			elseif ($array[1] == 'truefalse'){
				echo ('<center><p>Points: 50</p></center>
					<br><button class="right" value="50');
			}
			echo (',' . $array[0] . '">OK</button></center></div>');
		} 
		// if answer is wrong, show right answer
		elseif ($array[3] != $array[4]) {
			echo ('<center><p><span class="wrong_answer"><font size="35">Your Answer Is Wrong!</font></span></p> 
				<img src="images/wrong.png" width="150px" height="150px" margin-footer="10px"></img></center>
				<center><p>Punishment: -10</p></center>
				<p class="enunciation">The right answer is: ' .$array[5]  . '</p>
				<center><br><button class="wrong">OK</button></center></div>');
		} 
	}
}



// process question of type multichoice
$data = $_POST['checkbox'];
$array = explode(',', $data[0]);

if ($array[1] == 'multichoice'){

		// verify if there are answers selected
		$n = count($data);
		if ($n == 0 ){
			$conta = count($data);
				echo ('<center><p>You Did Not Choose a Option!</p>
					<button class="wrong">OK</button></center></div>');
		}
		else {
			$array = explode(',', $data[0]);
			if ($array[1] = 'multichoice'){
				//number of points for each right choice
				$score = ceil(100 / $array [2]);

				$n = $n - 1;

				// array of ids selected
				$answers = array();

				while ($n >= 0) {
					$tmp = explode(',', $data[$n]);
					$answers[] = $tmp[4]; 
				 	$n--;
				} 

				// array of right ids
				$right_idx = array();

				for ($j = 5; $j < count($tmp) ; $j++){
					$right_idx[] = $tmp[$j];
					$j++;
				}

				// count how many answers are right
				$count_right = 0;

				// count how many answers are wrong

				$count_wrong = 0;


				$aux = 0;

				for ($i = 0; $i < count($answers); $i++){
					$aux = $count_right;
					for ($j = 0; $j < $tmp[2] ; $j++){
						if ($answers[$i] == $right_idx[$j]){
							$count_right++;
						}
					}
					if ($aux == $count_right){
						$count_wrong++;
					}
				}

				//calculate points gain
				$points = $count_right * $score;

				//calculate points lost
				$punishment = $count_wrong * (-10);

				$total = $points + $punishment;


				if ($count_right == $tmp[2] && $punishment == 0){
					echo ('<center><p><span class="right_answer"><font size="35">Your Answer Is Right!</font></span></p>
					<img src="images/right.png" width="150px" height="150px" margin-footer="10px"></img>
					<center><p>Points: 100</p></center>
					<br/><button class="right" value="100,' . $tmp[0] . '">OK</button></div></center>');
				}
				elseif ($count_right == 0) {
					echo ('<center><p><span class="wrong_answer"><font size="35">Your Answer Is Wrong!</font></span></p> 
						<img src="images/wrong.png" width="150px" height="150px" margin-footer="10px"></img></center>
						<center><p>Punishment: -10</p></center>
						<p>The right answer are: <ul>');
					for ($i=6 ; $i < count($tmp) ; $i++){
						echo ('<li>' .$tmp[$i]  . '</li>');
						$i++;
					}

					echo('</ul><center><br><button class="wrong">OK</button></center></div>');
				}
				else {
					echo ('<center><p><span class="partially"><font size="28">Your Answer Is Partially Right!</font></span></p>
						<p>You advance, but you dont get the full points of question!</p>
						<img src="images/parcial.png" width="150px" height="150px" margin-footer="10px"></img></center>
						<center><p>Points: ' .$points .' | Punishment: ' . $punishment . ' | Total: ' . $total .'</p></center>
						<p class="enunciation">The right answers are: <ul>');

					for ($i=6 ; $i < count($tmp) ; $i++){
						echo ('<li>' .$tmp[$i]  . '</li>');
						$i++;
					}

					echo ('</ul><center><br><button class="right" value="' . $total . ',' . $tmp[0] . '">OK</button></center></div>');
				}
			}
		}
}

// if there is no answer choose
if (empty($_POST['checkbox']) && empty($data = $_POST['radio'])){
	echo ('<center><h1>You Did Not Choose a Option!</h1>
				<h3>You Lost This Question</h3>
				<button class="empty">Ok</button></center></div>');	
}


?>