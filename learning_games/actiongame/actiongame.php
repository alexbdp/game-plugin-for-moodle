<?php

/// DB HELPER TESTER
// Enable error logging: 
error_reporting(E_ALL ^ E_NOTICE);

// path to the db helper file
$tags = parse_ini_file(__DIR__ . "/../../config.ini"); 
$root = $tags['root'];
$dbhelper_file = $root . '/blocks/games/dbhelper.php';
			
// include the dbhelper file 
include_once $dbhelper_file;
// new dbhelper class 
$db_helper = new dbHelper();
// debugs db connection info
$db_helper->db_print_info();
// connects to the database provided in the constructor
$db_helper->db_connect();

// test of ids (user and course)
echo '<br>###### IDs TEST ######';
// tests aux function get_userID() that returns the current logged user id on the db
$userid = $db_helper->get_userID();
echo '<br>user id: '.$userid;
$username = $db_helper->db_get_user_name($userid);
echo '<br>user name: '.$username;
// tests aux function get_courseID() that returns the current course id on the db
$courseid = $db_helper->get_courseID();
echo '<br>course id: '.$courseid;
echo '<br>#######################<br>';
// tests aux function get_multichoice_questions() that returns an array of multichoice question objects
// require multichoice question class (once, avoiding redeclaration)
require_once $root . '/blocks/games/obj/multichoice_question.php';
$mc_questions = $db_helper->get_multichoice_questions();

// debug $mc_questions returned from db_helper
echo '<br> ###### MULTICHOICE QUESTIONS INFO ###### <br>';

foreach($mc_questions as $mc_question)
{
	echo '<br>-- Question: '.$mc_question->name;
	echo '<br>- Text: '.$mc_question->text;
	echo '<br>- Possible Answers: ';
	
	for($i = 0; $i < count($mc_question->answers); $i++)
	{
		echo '<br>'.($i+1).') '.$mc_question->answers[$i];
	}
	
	echo '<br>- Correct Answers: ';
	
	foreach($mc_question->correct_answers_idx as $correct_answer)
	{
		echo '<br>'.($correct_answer+1).') '.$mc_question->answers[$correct_answer];
	}
	echo '<br>';
}
echo '<br> ###########################################';
// end of $mc_questions debug

// tests aux function db_get_players() that returns an array of players objects
// require players class (once, avoiding redeclaration)



echo '<br>';
echo '<br> ###### GAMES INFO ###### <br>';

// games folder
$path_games = __DIR__ ;

// path to the description file of the game, containing all
// info that we`ll be loading on our data structure
$description_file = $path_games . '/description.php';

// include the game description file to read its vars
include $description_file;

// stores the remaining info of the game on the data structure
$game = $name;

echo '<br>- Tipo do Jogo: '. $game;

$game_typeID = $db_helper->db_get_game_typeID($game);
echo '<br>- Id do tipo de game: '. $game_typeID;

$db_helper->db_register_game($game_typeID,'Primeira fase');
$db_helper->db_register_game($game_typeID,'Segunda fase');
$db_helper->db_register_game($game_typeID,'Terceira fase');


require_once $root . '/blocks/games/obj/current_game.php';

echo '<br>- Games existentes: ';

$games = $db_helper->db_get_game_of_type($game_typeID);


foreach($games as $game)
{
	echo '<br>';
	echo '<br>- Name: '.$game->name;
	echo '<br>- Game ID: '.$game->gameid;
	echo '<br>- Game Type ID: '.$game->gametypeid;
	echo '<br>';
};

echo '<br>- Games do curso: ';

$db_helper->db_register_game_course($games[1]->gameid,$courseid);


$game_course = $db_helper->db_get_game_of_course($game_typeID,$courseid);

$name_game = 'Exterminador';

$game_id = $db_helper->db_get_gameID($name_game);

echo 'ID do Jogo:' . $name_game . ' - ' . $game_id;

foreach($game_course as $gc)
{
	echo '<br>';
	echo '<br>- Name: '.$gc->name;
	echo '<br>- Game ID: '.$gc->gameid;
	echo '<br>- Game Type ID: '.$gc->gametypeid;
	echo '<br>';
};


echo '<br> ###########################################';



echo '<br>';
echo '<br> ###### PLAYERS INFO ###### <br>';


$game_courseID = $db_helper->db_get_game_courseID($games[1]->gameid,$courseid);

$db_helper->db_register_player($userid,$game_courseID);

$teste = $db_helper->db_get_player_info($game_courseID);

foreach($teste as $player)
{
	echo '<br>';
	echo '<br> Name: ' . $player->name;
	echo '<br> ID: ' . $player->playerid;
	echo '<br> User ID: ' . $player->userid;
	echo '<br> Game ID: ' . $player->gameid;
	echo '<br> Score: ' . $player->score;
	echo '<br>';
};


echo '<br> ###### LISTA QUIZ INFORMAÇÕES ###### <br>';


require_once $root . '/blocks/games/obj/quiz.php';

$quizes = $db_helper->db_get_quiz_of_course($courseid);

foreach($quizes as $quiz)
{
	echo '<br>';
	echo '<br> Name: ' . $quiz->name;
	echo '<br> ID: ' . $quiz->id;
	echo '<br>';
};